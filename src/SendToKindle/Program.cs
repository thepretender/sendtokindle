﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;

namespace SendToKindle
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CultureInfo.CurrentCulture = new CultureInfo("en-us");
            CultureInfo.CurrentUICulture = CultureInfo.CurrentCulture;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.CurrentCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.CurrentCulture;

            if (args.Length == 0)
            {
                Usage();
                return;
            }

            string inPath = args[0];
            if (!File.Exists(inPath))
            {
                Console.WriteLine($"Can't find input file: {inPath}");
                return;
            }

            string outPath;
            if (string.Equals(Path.GetExtension(inPath), ".mobi", StringComparison.OrdinalIgnoreCase))
            {
                outPath = inPath;
            }
            else
            {
                outPath = Path.Combine(Path.GetDirectoryName(inPath), Path.GetFileNameWithoutExtension(inPath) + ".mobi");
                ConvertBook(inPath, Path.GetFileName(outPath));

                if (!File.Exists(outPath) || new FileInfo(outPath).Length == 0)
                {
                    Console.WriteLine("Failed to convert file, exiting.");
                    return;
                }
            }

            Console.WriteLine("Sending the book.");
            SendBook(outPath);
            Console.WriteLine("Book sent.");
        }

        private static void SendBook(string path)
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("config.json");
            var config = builder.Build();

            string server = config["Mail:Server"];
            int port = Convert.ToInt32(config["Mail:Port"]);
            string login = config["Mail:Login"];
            string password = config["Mail:Password"];
            string from = config["Mail:From"];
            string to = config["KindleEmail"];

            if (string.IsNullOrEmpty(server))
            {
                Console.WriteLine("Mail server is not specified!");
                return;
            }

            if (port == 0)
            {
                Console.WriteLine("Mail port is not specified!");
                return;
            }

            if (string.IsNullOrEmpty(login))
            {
                Console.WriteLine("Mail login is not specified!");
                return;
            }

            if (string.IsNullOrEmpty(password))
            {
                Console.WriteLine("Mail password is not specified!");
                return;
            }

            if (string.IsNullOrEmpty(from))
            {
                Console.WriteLine("Mail From is not specified!");
                return;
            }

            if (string.IsNullOrEmpty(to))
            {
                Console.WriteLine("Kindle Email is not specified!");
                return;
            }

            string fileName = Path.GetFileName(path);
            var attachment = new MimePart(ContentType.Parse("application/x-mobipocket-ebook"))
                             {
                                 ContentObject = new ContentObject(File.OpenRead(path)),
                                 ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                                 ContentTransferEncoding = ContentEncoding.Base64,
                                 FileName = fileName
                             };
            var body = new TextPart("plain") {Text = $"Hey dudes, here's the book: {fileName}"};
            var multipart = new Multipart("mixed");
            multipart.Add(body);
            multipart.Add(attachment);
            var msg = new MimeMessage
                      {
                          From = { new MailboxAddress("", from)},
                          To = { new MailboxAddress("", to)},
                          Body = multipart,
                          Subject = $"New book: {fileName}"
            };

            using (var client = new SmtpClient())
            {
                client.Connect(server, port, SecureSocketOptions.StartTls);
                client.Authenticate(login, password);
                client.Send(msg);
                client.Disconnect(true);
            }
        }

        private static void Usage()
        {
            Console.WriteLine("SendToKindle usage:");
            Console.WriteLine("\tsendtokindle filename");
        }

        private static void ConvertBook(string inPath, string outPath)
        {
            string genPath = Path.Combine(AppContext.BaseDirectory, "kindlegen.exe");

            if (!File.Exists(genPath))
            {
                UnpackeConverter(genPath);
            }

            Console.WriteLine("Converting file...");

            var process = new Process();

            process.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data);
            process.StartInfo = new ProcessStartInfo
                                {
                                    FileName = "kindlegen.exe",
                                    WorkingDirectory = Directory.GetCurrentDirectory(),
                                    Arguments = $"\"{inPath}\" -c2 -o \"{outPath}\"",
                                    UseShellExecute = false,
                                    RedirectStandardOutput = true,
                                    StandardOutputEncoding = Encoding.UTF8
                                };
            process.Start();
            process.BeginOutputReadLine();
            process.WaitForExit();
            process.CancelOutputRead();
        }

        private static void UnpackeConverter(string path)
        {
            using (var inStream = Assembly.GetEntryAssembly().GetManifestResourceStream("SendToKindle.Resources.kindlegen.exe"))
            using (var outStream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                inStream.CopyTo(outStream);
            }
        }
    }
}
